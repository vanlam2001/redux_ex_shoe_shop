import React, { Component } from 'react'
import { connect } from 'react-redux'

class ItemShoe extends Component {
    render() {
        let { image, name, price } = this.props.shoe
        return (
            <div>
                <div className="card col-10 " style={{ width: '18rem' }}>
                    <img src={image} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{name}</h5>
                        <p className="card-text">{price}</p>
                        <a onClick={() => this.props.themVaoGioHang(this.props.shoe)} href="#" className="btn btn-primary">Add to cart</a>
                    </div>
                </div>
            </div>
        )
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        themVaoGioHang: (shoe) => {
            let action = {
                type: "ADD_TO_CART",
                payload: shoe,
            };
            dispatch(action);
        }
    }
}

export default connect(null, mapDispatchToProps)(ItemShoe);
