import React, { Component } from 'react'
import ItemShoe from './ItemShoe'
import { connect } from 'react-redux'

class ListShoe extends Component {
    render() {
        return (
            <div>
                <div className="row">
                    {
                        this.props.list.map((item) => {
                            return <ItemShoe shoe={item}></ItemShoe>
                        })
                    }
                </div>
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        list: state.shoeReducer.listShoe,
    };
};

export default connect(mapStateToProps)(ListShoe)