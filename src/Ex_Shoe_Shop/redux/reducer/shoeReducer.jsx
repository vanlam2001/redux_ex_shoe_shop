import { data_shoe } from "../../data_shoe";

let initialValue = {
    listShoe: data_shoe,
    cart: [],
};

export const shoeReducer = (state = initialValue, action) => {
    switch (action.type) {
        case "ADD_TO_CART": {
            let cloneCart = [...state.cart];
            let index = cloneCart.findIndex((item) => {
                return item.id == action.payload.id;
            })

            if (index == -1) {
                let newShoe = { ...action.payload, soLuong: 1 };
                cloneCart.push(newShoe);
            }

            else {
                cloneCart[index].soLuong++;
            }

            return { ...state, cart: cloneCart }
        }

        case "DELETE_SHOE": {
            let newCart = state.cart.filter((item) => {
                return item.id != action.payload;
            });
            return { ...state, cart: newCart };
        }

        case "CHANGE_QUALITY": {
            let cloneCart = [...state.cart];

            let index = cloneCart.findIndex((item) => {
                return item.id == action.payload.idShoe;
            })
            cloneCart[index].soLuong += action.payload.luaChon;
            return { ...state, cart: cloneCart }
        }



        default:
            return state;
    }
};