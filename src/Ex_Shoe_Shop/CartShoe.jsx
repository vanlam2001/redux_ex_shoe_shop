import React, { Component } from 'react'
import { connect } from 'react-redux'

class CartShoe extends Component {

    renderTbody = () => {
        return this.props.cart.map((item) => {
            let { id, name, price, image, soLuong } = item;
            return (
                <tr>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>{price * soLuong}</td>
                    <td>
                        <img style={{ width: 50 }} src={image} alt="" />
                    </td>
                    <td>
                        <button onClick={() => this.props.chinhSuaSoLuong(item.id, -1)} className='btn-outline-primary mr-2'>-</button>
                        <strong>{soLuong}</strong>
                        <button onClick={() => this.props.chinhSuaSoLuong(item.id, +1)} className='btn btn-outline-primary ml-2'>+</button>
                    </td>
                    <td>
                        <button onClick={() => {
                            this.props.chucNangXoa(item.id)
                        }} className='btn btn-danger'>Xóa</button>
                    </td>
                </tr>
            )
        })
    }

    render() {
        return (
            <div>
                <div className="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Img</th>
                            <th>Số lượng</th>
                            <th>Chức năng</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderTbody()}
                    </tbody>
                </div>
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        cart: state.shoeReducer.cart,
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        chucNangXoa: (idShoe) => {
            let action = {
                type: "DELETE_SHOE",
                payload: idShoe
            }
            dispatch(action);
        },

        chinhSuaSoLuong: (idShoe, luaChon) => {
            let action = {
                type: "CHANGE_QUALITY",
                payload: { idShoe: idShoe, luaChon }
            };
            dispatch(action);
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CartShoe)